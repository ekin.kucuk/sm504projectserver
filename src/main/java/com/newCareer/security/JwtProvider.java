package com.newCareer.security;

import org.springframework.stereotype.Component;
import com.newCareer.model.GenericSysUser;
import io.jsonwebtoken.*;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.security.core.Authentication;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtProvider {
	
	private static final long EXPIRATION_TIME = 864000_000;
	private static final String SECRET ="SecretKeyToGenJWTs";
	

	public String generateToken(Authentication authentication) {

		GenericSysUser genericSysUser = (GenericSysUser) authentication.getPrincipal();
		Date now = new Date(System.currentTimeMillis());
		Date expiryDate = new Date(now.getTime() + EXPIRATION_TIME);
		String userId = Long.toString(genericSysUser.getId());
		Map<String, Object> claims = new HashMap<>();
		claims.put("id", userId);
		claims.put("email", genericSysUser.getEmail());
	
		
		 return Jwts.builder()
	                .setSubject(userId)
	                .setClaims(claims)
	                .setIssuedAt(now)
	                .setExpiration(expiryDate)
	                .signWith(SignatureAlgorithm.HS512, SECRET)
	                .compact();

	}
	
	public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
            return true;
        }catch (SignatureException ex){
            System.out.println("Invalid JWT Signature");
        }catch (MalformedJwtException ex){
            System.out.println("Invalid JWT Token");
        }catch (ExpiredJwtException ex){
            System.out.println("Expired JWT token");
        }catch (UnsupportedJwtException ex){
            System.out.println("Unsupported JWT token");
        }catch (IllegalArgumentException ex){
            System.out.println("JWT claims string is empty");
        }
        return false;
    }
	
	public Long getUserIdFromJWT(String token){
        Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        String id = (String)claims.get("id");

        return Long.parseLong(id);
    }

}
