package com.newCareer.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.util.StringUtils;
import com.newCareer.model.GenericSysUser;
import com.newCareer.service.GenericSysUserService;

public class JwtAuthenticationFilter extends OncePerRequestFilter {
	
	private static final String TOKEN_PREFIX = "Bearer ";
	private static final String HEADER_STRING = "Authorization";
	
	@Autowired
	private JwtProvider jwtProvider;
	
	@Autowired
	private GenericSysUserService genericSysUserService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		 String jwt = getJWTFromRequest(request);
		 if(StringUtils.hasText(jwt)&& jwtProvider.validateToken(jwt)){
             Long userId = jwtProvider.getUserIdFromJWT(jwt);
             
             GenericSysUser user = genericSysUserService.getGenericUSerById(userId);
             
             UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
            		 user, null, Collections.emptyList());
             authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
             SecurityContextHolder.getContext().setAuthentication(authentication);
             
		 }
		 
		 filterChain.doFilter(request, response);

	}
	
	 private String getJWTFromRequest(HttpServletRequest request){
	        String bearerToken = request.getHeader(HEADER_STRING);

	        if(StringUtils.hasText(bearerToken)&&bearerToken.startsWith(TOKEN_PREFIX)){
	            return bearerToken.substring(7, bearerToken.length());
	        }

	        return null;
	    }
	

}
