package com.newCareer.payload;

public class JobApplicationPayload {
	
	private Long jobId;
	
	private Long userId;
	
    private String date;

    public JobApplicationPayload() {
    	
    }
    
	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
    
    

}
