package com.newCareer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.newCareer.model.UserDetail;

@Repository
public interface UserDetailRepository extends CrudRepository<UserDetail, Long> {
	
	@Query(value="select * from user_detail where id = ?1",nativeQuery= true)
	public UserDetail getUSerById(Long id);
	
	@Query(value = "select * from user_detail where generic_sys_user_id = ?1",nativeQuery = true)
	public UserDetail getUserBySysId(Long id);

}
