package com.newCareer.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.newCareer.model.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long>{
	
	@Query(value="select * from company where id = ?1",nativeQuery = true)
	public Company getCompanyById(Long id);
	
	@Query(value="select * from company where generic_sys_user_id = ?1",nativeQuery=true)
    public Company getCompanyByUseId(Long id);
	
}
 