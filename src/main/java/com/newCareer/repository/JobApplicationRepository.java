package com.newCareer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.newCareer.model.JobApplication;

@Repository
public interface JobApplicationRepository extends CrudRepository<JobApplication, Long> {
	
	
	
	@Query(value = "select * from job_application where  user_detail_id = ?1",nativeQuery = true)
	public List<JobApplication> getJobApplicationsofaUser(Long userId);
	
	@Query(value = "select user_detail_id from job_application where job_posting_id = ?1",nativeQuery = true)
	public List<Long> getListofApplicants(Long jobPostingID);

}
