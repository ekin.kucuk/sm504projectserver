package com.newCareer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.newCareer.model.JobPosting;

@Repository
public interface JobPostingRepository extends CrudRepository<JobPosting,Long>{
	
	@Query(value="select * from job_posting",nativeQuery = true)
	public List<JobPosting> getAllJobPostings();
    
	@Query(value="select * from job_posting where company_id = ?1",nativeQuery = true)
	public List<JobPosting> getlJobPostingsByCompanyId(Long companyId);
	
	@Query(value="select * from job_posting where id = ?1",nativeQuery = true)
	public JobPosting getJobPostingById(Long id);
	
}
