package com.newCareer.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.newCareer.model.GenericSysUser;

@Repository
public interface GenericSysUserRepository extends CrudRepository<GenericSysUser, Long> {
	
	@Query(value = "select * from generic_sys_user where id = ?1",nativeQuery = true)	
	public GenericSysUser getGenericUSerById(Long id);
	
	@Query(value = "select * from generic_sys_user where email = ?1",nativeQuery = true)
	public GenericSysUser getUserByEmail(String email);

}
