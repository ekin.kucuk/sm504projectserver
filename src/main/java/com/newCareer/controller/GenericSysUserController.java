package com.newCareer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newCareer.payload.CompanyPayload;
import com.newCareer.payload.JWTLoginSuccessResponse;
import com.newCareer.payload.LoginRequest;
import com.newCareer.payload.UserPayload;
import com.newCareer.security.JwtProvider;
import com.newCareer.service.GenericSysUserService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class GenericSysUserController {

	@Autowired
	private GenericSysUserService genericSysUserService;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
    private JwtProvider tokenProvider;
	
	private static final String TOKEN_PREFIX= "Bearer ";

	@PostMapping("/company/signup")
	public ResponseEntity<?> companySignUp(@RequestBody CompanyPayload payload) {

		return genericSysUserService.saveCompanyUser(payload);

	}
	
	@PostMapping("/user/signup")
	public ResponseEntity<?> userSignup(@RequestBody UserPayload userPayload){
		return genericSysUserService.saveUserDetail(userPayload);
	}

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {

		String email = loginRequest.getEmail();
		String password = loginRequest.getPassword();

		if (email == null || password == null) {
			return new ResponseEntity<String>("Missing email or password", HttpStatus.BAD_REQUEST);
		} else {
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(email, password));
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
	        String jwt = TOKEN_PREFIX +  tokenProvider.generateToken(authentication);

	        return new  ResponseEntity<>(new JWTLoginSuccessResponse(true, jwt),HttpStatus.OK);
		}
	}

}
