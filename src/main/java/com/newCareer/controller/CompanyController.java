package com.newCareer.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newCareer.model.Company;
import com.newCareer.service.CompanyService;

@RestController
@RequestMapping("/company")
@CrossOrigin
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@GetMapping("/byuser/{id}")
	public ResponseEntity<?> getCompanyByUserId(@PathVariable Long id, Principal principal) {
		Company c = companyService.findCompanyByuserId(id);

		if (c == null) {
			return new ResponseEntity<String>("Null", HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Company>(c, HttpStatus.OK);
		}

	}

	@GetMapping("/hello/{id}")
	public ResponseEntity<?> printHello(@PathVariable Long id) {
		Company c = companyService.findCompanyByuserId(id);

		if (c == null) {
			return new ResponseEntity<String>("Null", HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Company>(c, HttpStatus.OK);
		}

	}

}
