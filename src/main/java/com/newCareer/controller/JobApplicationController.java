package com.newCareer.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newCareer.payload.JobApplicationPayload;
import com.newCareer.service.JobApplicationService;

@RestController
@RequestMapping("/jobapplication")
@CrossOrigin
public class JobApplicationController {
	
	@Autowired
	private JobApplicationService jobApplicationService;
	
	@PostMapping("/create")
	public ResponseEntity<?> makeaJobApplication(@RequestBody JobApplicationPayload payload,Principal principal){
		
		return this.jobApplicationService.makeJobApplication(payload);
	}
	
	@GetMapping("/byuser/{userId}")
	public ResponseEntity<?> getJobApplicationsByUserId(@PathVariable Long userId,Principal principal){
		return this.jobApplicationService.getJobApplicationsofaUser(userId);
	}

	@GetMapping("/getapplicants/{jobId}")
	public ResponseEntity<?> getApplicants(@PathVariable Long jobId, Principal principal){
		return this.jobApplicationService.getListofApplicants(jobId);
	}
	
	
	
}
