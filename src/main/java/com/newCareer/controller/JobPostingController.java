package com.newCareer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newCareer.payload.JobPayload;
import com.newCareer.service.JobPostingService;
import java.security.Principal;

@RestController
@RequestMapping("/job")
@CrossOrigin
public class JobPostingController {
	
	@Autowired
	private JobPostingService jobPostingService;
	
	
	@PostMapping("/create")
	public ResponseEntity<?> createJobPosting(@RequestBody JobPayload job, Principal principal){
		
		return jobPostingService.createJobPosting(job);
	}
	
	@GetMapping("/getall")
	public ResponseEntity<?> getAllJobPostings(){
		return jobPostingService.getAllJobPostings();
	}
	
	
	@GetMapping("/getByCompanyId/{companyId}")
	public ResponseEntity<?> getJobPostingsByCompanyId(@PathVariable Long companyId){
		return jobPostingService.getJobPostingsByCompanyId(companyId);
	}

}
