package com.newCareer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.newCareer.model.UserDetail;
import com.newCareer.service.UserDetailService;

@RestController("/user")
@CrossOrigin
public class UserDetailController {
	
	@Autowired
	private UserDetailService userDetailService;
	
	
	@GetMapping("/byid/{id}")
	public UserDetail getUserById(@PathVariable Long id) {
		return userDetailService.getUSerById(id);
	}

}
