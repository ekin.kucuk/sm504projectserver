package com.newCareer.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class UserDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "generic_sys_user_id")
	private GenericSysUser genericSysUser;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="user_detail_id")
	private List<JobApplication> jobApplications;
	
	private String name;
	
	private String surname;
	
	private String jobTitle;
	
	private String pastExperience;
	
	private String education;

	public UserDetail() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	
	public String getPastExperience() {
		return pastExperience;
	}

	public void setPastExperience(String pastExperience) {
		this.pastExperience = pastExperience;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public GenericSysUser getGenericSysUser() {
		return genericSysUser;
	}

	public void setGenericSysUser(GenericSysUser genericSysUser) {
		this.genericSysUser = genericSysUser;
	}

	public List<JobApplication> getJobApplication() {
		return jobApplications;
	}

	public void setJobApplication(List<JobApplication> jobApplication) {
		this.jobApplications = jobApplication;
	}
	
	
	
	

}
