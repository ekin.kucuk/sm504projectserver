package com.newCareer.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.newCareer.model.Company;
import com.newCareer.model.GenericSysUser;
import com.newCareer.model.UserDetail;
import com.newCareer.payload.CompanyPayload;
import com.newCareer.payload.UserPayload;
import com.newCareer.repository.GenericSysUserRepository;

@Service
public class GenericSysUserService implements UserDetailsService {

	@Autowired
	private GenericSysUserRepository genericSysUserRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserDetailService userDetailService;

	public GenericSysUser getGenericUserByEmail(String email) {

		return genericSysUserRepository.getUserByEmail(email);

	}

	public GenericSysUser getGenericUSerById(Long id) {
		return genericSysUserRepository.getGenericUSerById(id);
	}

	public GenericSysUser saveGenericUSer(String email, String password) {

		GenericSysUser existingUser = this.getGenericUserByEmail(email);
		if (existingUser != null) {
			return null;
		} else {

			GenericSysUser newUser = new GenericSysUser();
			newUser.setPassword(bCryptPasswordEncoder.encode(password));
			newUser.setEmail(email);
			return genericSysUserRepository.save(newUser);

		}

	}

	public ResponseEntity<?> saveCompanyUser(CompanyPayload companyPayload) {

		String email = companyPayload.getEmail();
		String password = companyPayload.getPassword();

		if (email == null || password == null) {
			return new ResponseEntity<String>("Missing Email or Password", HttpStatus.BAD_REQUEST);
		} else {
			GenericSysUser userCheck = this.saveGenericUSer(email, password);

			if (userCheck == null) {
				return new ResponseEntity<String>("User already exist", HttpStatus.BAD_REQUEST);
			} else {

				Company newCompany = companyService.createNewCompany(companyPayload, userCheck);

				return new ResponseEntity<Company>(newCompany, HttpStatus.OK);
			}
		}

	}

	public ResponseEntity<?> saveUserDetail(UserPayload userPayload) {
		String email = userPayload.getEmail();
		String password = userPayload.getPassword();

		if (email == null || password == null) {
			return new ResponseEntity<String>("Missing Email or Password", HttpStatus.BAD_REQUEST);
		} else {
			GenericSysUser userCheck = this.saveGenericUSer(email, password);
			if (userCheck == null) {
				return new ResponseEntity<String>("User already exist", HttpStatus.BAD_REQUEST);
			} else {
				UserDetail newUser = userDetailService.createNewUserAccount(userPayload, userCheck);
				return new ResponseEntity<UserDetail>(newUser, HttpStatus.OK);
			}
		}

	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		return (UserDetails) genericSysUserRepository.getUserByEmail(username);
	}

}
