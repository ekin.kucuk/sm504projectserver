package com.newCareer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newCareer.model.GenericSysUser;
import com.newCareer.model.UserDetail;
import com.newCareer.payload.UserPayload;
import com.newCareer.repository.UserDetailRepository;

@Service
public class UserDetailService {
	
	@Autowired
	private UserDetailRepository userDetailRepository;
	
	
	public UserDetail getUSerById(Long id) {
		
		return userDetailRepository.getUSerById(id);
	}
	
	public UserDetail createNewUserAccount(UserPayload userPayload, GenericSysUser genericUser) {
		
		UserDetail newUser = new UserDetail();
		
		newUser.setEducation(userPayload.getEducation());
		newUser.setPastExperience(userPayload.getPastExperience());
		newUser.setJobTitle(userPayload.getJobTitle());
		newUser.setName(userPayload.getName());
		newUser.setSurname(userPayload.getSurname());
		newUser.setGenericSysUser(genericUser);
		
		return userDetailRepository.save(newUser);
	}
	

}
