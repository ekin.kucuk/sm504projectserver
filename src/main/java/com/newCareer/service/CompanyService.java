package com.newCareer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newCareer.model.Company;
import com.newCareer.model.GenericSysUser;
import com.newCareer.payload.CompanyPayload;
import com.newCareer.repository.CompanyRepository;

@Service
public class CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	
	public Company findCompanyById(Long id) {

		return companyRepository.getCompanyById(id); 
	}

	public Company createNewCompany(CompanyPayload companyPayload, GenericSysUser genericUser) {
		
		Company newCompany = new Company();
		
		newCompany.setAbout(companyPayload.getAbout());
		newCompany.setCountry(companyPayload.getCountry());
		newCompany.setCity(companyPayload.getCity());
		newCompany.setCompanyName(companyPayload.getCompanyName());
		newCompany.setGenericSysUser(genericUser);
		
		return companyRepository.save(newCompany);
	}
	
	
	public Company findCompanyByuserId(Long id) {
		
		return companyRepository.getCompanyByUseId(id);
	}
	
	
}
