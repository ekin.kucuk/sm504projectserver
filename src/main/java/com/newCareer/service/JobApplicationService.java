package com.newCareer.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.newCareer.model.Company;
import com.newCareer.model.JobApplication;
import com.newCareer.model.JobPosting;
import com.newCareer.model.UserDetail;
import com.newCareer.payload.JobApplicationPayload;
import com.newCareer.repository.JobApplicationRepository;
import com.newCareer.repository.JobPostingRepository;
import com.newCareer.repository.UserDetailRepository;

@Service
public class JobApplicationService {
	
	@Autowired
	private JobApplicationRepository jobApplicationRepository;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private JobPostingService jobPostingService;
	
	@Autowired 
	private CompanyService companyService;
	
	@Autowired
	private JobPostingRepository jobPostingRepository;
	
	@Autowired
	private UserDetailRepository userDetailRepositoy;
	
	private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public ResponseEntity<?> makeJobApplication(JobApplicationPayload payload){
		
		Long userId = payload.getUserId();
		
		Long jobId = payload.getJobId();
		
		if(jobId == null || userId == null) {
			return new ResponseEntity<String>("Missing info",HttpStatus.BAD_REQUEST);
		}
		
		Date date = new Date();
		
		String dateStr = formatter.format(date);
		
		JobApplication application = new JobApplication();
		
		UserDetail user = userDetailRepositoy.getUserBySysId(userId);
		
		JobPosting job = jobPostingRepository.getJobPostingById(jobId);
		
		Company comp = job.getCompany();
		String applicationInfo = comp.getCompanyName()+" "+job.getPositionName();
		
		application.setDate(dateStr);
		application.setJobPosting(job);
		application.setUserDetail(user);
		application.setApplicationInfo(applicationInfo);
		
		application = jobApplicationRepository.save(application);
		
		return new ResponseEntity<JobApplication>(application,HttpStatus.OK); 
		
				
	}
	
	
	public ResponseEntity<?> getJobApplicationsofaUser(Long userId){
		
		if(userId == null) {
			return new ResponseEntity<String>("Missing info",HttpStatus.BAD_REQUEST);
		}
		
		UserDetail user = userDetailRepositoy.getUserBySysId(userId);
		
		Long userDetailId = user.getId();
		List<JobApplication> applications = jobApplicationRepository.getJobApplicationsofaUser(userDetailId);
		
		return new ResponseEntity<List<JobApplication>>(applications,HttpStatus.OK);
	}
	
	
	public ResponseEntity<?> getListofApplicants(Long JobPostingId){
		
		List <Long> userIds = this.jobApplicationRepository.getListofApplicants(JobPostingId);
		
		List<UserDetail> users = new ArrayList<>();
		
		for (Long id : userIds) {
			
			UserDetail user = userDetailRepositoy.getUSerById(id);
			
			users.add(user);
			
		}
		
		
		return new ResponseEntity<List<UserDetail>>(users,HttpStatus.OK);
		
	}
	
	
}
