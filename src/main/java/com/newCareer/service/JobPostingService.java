package com.newCareer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.newCareer.model.Company;
import com.newCareer.model.JobPosting;
import com.newCareer.payload.JobPayload;
import com.newCareer.repository.JobPostingRepository;

@Service
public class JobPostingService {

	@Autowired
	private JobPostingRepository jobPostingRepository;

	@Autowired
	private CompanyService companyService;

	public ResponseEntity<?> createJobPosting(JobPayload job) {

		Long companyId = job.getCompanyId();
		if (companyId == null) {
			return new ResponseEntity<String>("Missing company info", HttpStatus.BAD_REQUEST);
		} else {

			Company existingCompany = companyService.findCompanyById(companyId);
			if (existingCompany == null) {
				return new ResponseEntity<>("Wrong company info", HttpStatus.BAD_REQUEST);
			} else {
				JobPosting newPosting = new JobPosting();

				newPosting.setCompany(existingCompany);
				newPosting.setDescription(job.getDescription());
				newPosting.setPositionName(job.getPositionName());

				JobPosting created = jobPostingRepository.save(newPosting);

				return new ResponseEntity<JobPosting>(created, HttpStatus.OK);
			}

		}

	}
	
	
	public ResponseEntity<?> getAllJobPostings(){
		
		List<JobPosting> allJobs = jobPostingRepository.getAllJobPostings();
		
		if(allJobs == null) {
			return new ResponseEntity<String>("Cannot find any job entry",HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<JobPosting>>(allJobs,HttpStatus.OK);
		
	}
	
	public ResponseEntity<?> getJobPostingsByCompanyId(Long companyId){
		
		if(companyId == null) {
			return new ResponseEntity<String>("Missing company info", HttpStatus.BAD_REQUEST);
		}
			
		List<JobPosting> jobs = jobPostingRepository.getlJobPostingsByCompanyId(companyId);
		if(jobs == null) {
			return new ResponseEntity<String>("Cannot find any job entry",HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<JobPosting>>(jobs,HttpStatus.OK);
		
	}
	
	public JobPosting getJobById(Long id) {
		
		return jobPostingRepository.getJobPostingById(id);
	}
	
	

	

}
